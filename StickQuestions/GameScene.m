//
//  GameScene.m
//  StickQuestions
//
//  Created by Avery Lamp on 1/27/15.
//  Copyright (c) 2015 Avery Lamp. All rights reserved.
//

#import "GameScene.h"
#import "Joystick.h"
#import "SKCharacter.h"
@interface GameScene()<SKPhysicsContactDelegate>
@property Joystick *joystick;
@property SKSpriteNode *punchButton;
@property SKSpriteNode *kickButton;
@property CGSize screenSize;
@property int characterScale;
@property SKCharacter *mainCharacter;
@property SKCharacter *computerCharacter;
@property SKNode *mainCharacterWrapper;
@property SKNode *computerCharacterWrapper;
@property CGSize lastBodySize;
@property SKShapeNode *mainHealth;
@property SKShapeNode *computerHealth;
@end

@implementation GameScene

static const uint32_t mainCharacterCategory = 0x1 <<0;
static const uint32_t opposingCharacterCategory = 0x1 <<1;

-(void)didMoveToView:(SKView *)view {
    /* Setup your scene here */
    self.physicsWorld.contactDelegate = self;
    self.physicsWorld.gravity = CGVectorMake(0, -10);
    
    SKNode *ground = [SKNode node];
    ground.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(self.frame.size.width, self.frame.size.height/2)];
    
    SKPhysicsBody* borderBody = [SKPhysicsBody bodyWithEdgeLoopFromRect:CGRectMake(0, 300, self.frame.size.width, self.frame.size.height)];
    // 2 Set physicsBody of scene to borderBody
    self.physicsBody = borderBody;
    // 3 Set the friction of that physicsBody to 0
    self.physicsBody.friction = 0.3f;
    
    SKLabelNode *myLabel = [SKLabelNode labelNodeWithFontNamed:@"Chalkduster"];
    self.screenSize = self.frame.size;
    
    myLabel.text = @"Game Scene!";
    myLabel.fontSize = 65;
    myLabel.position = CGPointMake(CGRectGetMidX(self.frame),
                                   CGRectGetMidY(self.frame));
    //[self addChild:myLabel];
    SKSpriteNode *jsThumb = [SKSpriteNode spriteNodeWithImageNamed:@"joystick"];
    SKSpriteNode *jsBackdrop = [SKSpriteNode spriteNodeWithImageNamed:@"dpad"];
    self.joystick = [Joystick joystickWithThumb:jsThumb andBackdrop:jsBackdrop];
    self.joystick.position = CGPointMake(jsBackdrop.size.width, jsBackdrop.size.height);
    self.joystick.position = CGPointMake(CGRectGetMinX(self.frame) + 160, CGRectGetMinY(self.frame)+230);
    [self addChild:self.joystick];
    
    self.punchButton = [SKSpriteNode spriteNodeWithImageNamed:@"arcade_red_unpushed.png"];
    self.kickButton = [SKSpriteNode spriteNodeWithImageNamed:@"arcade_blue_unpushed.png"];
    self.punchButton.name = @"punchButton";
    self.kickButton.name = @"kickButton";
    self.punchButton.xScale = 0.6;
    self.punchButton.yScale = 0.6;
    self.kickButton.xScale = 0.6;
    self.kickButton.yScale = 0.6;
    
    self.punchButton.position = CGPointMake(self.screenSize.width - 140, 200);
    self.kickButton.position = CGPointMake(self.screenSize.width - 280, 200);
    
    [self  addChild:self.kickButton];
    [self addChild: self.punchButton];
    
    
    
    self.mainCharacterWrapper = [SKNode node];
    self.mainCharacter.characterName = @"ken";
    self.mainCharacter = [SKCharacter spriteNodeWithImageNamed:@"ken_idle_0.png"];
    self.mainCharacter.named = @"mainCharacter";
    self.mainCharacter.name = @"mainCharacter";
    self.mainCharacter.health = 100;
    
    self.mainCharacterWrapper.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:self.mainCharacter.texture.size];
    self.lastBodySize = self.mainCharacter.texture.size;
    
    
    self.mainCharacterWrapper.physicsBody.categoryBitMask = mainCharacterCategory;
    self.mainCharacterWrapper.physicsBody.dynamic = YES;
    self.mainCharacterWrapper.physicsBody.mass = 1;
    self.mainCharacterWrapper.physicsBody.friction =1;
    self.mainCharacterWrapper.physicsBody.allowsRotation = NO;
    
    
    
    self.characterScale = 3.5;
    self.mainCharacter.xScale =self.characterScale;
    self.mainCharacter.yScale =self.characterScale;
    self.mainCharacter.anchorPoint = CGPointMake(0.5, 1.0/self.characterScale);
    
    self.mainCharacter.currentState = @"idle";
    SKAction *idleAction = [GameScene SKActionAnimationWithCharacterName:@"ken" move:@"idle"];
    [self.mainCharacter runAction:[SKAction repeatAction:idleAction count:10]];
    self.mainCharacterWrapper.position = CGPointMake(CGRectGetMidX(self.frame)-200, CGRectGetMidY(self.frame));
    [self addChild:self.mainCharacterWrapper];
    [self.mainCharacterWrapper addChild:self.mainCharacter];
    
    self.self.computerCharacterWrapper = [SKNode node];
    
    self.self.computerCharacter = [SKCharacter spriteNodeWithImageNamed:@"ken_idle_0.png"];
    
    
    
    self.self.computerCharacterWrapper.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:self.computerCharacter.texture.size];
    self.computerCharacterWrapper.physicsBody.categoryBitMask = opposingCharacterCategory;
    
    self.computerCharacterWrapper.physicsBody.contactTestBitMask = opposingCharacterCategory|mainCharacterCategory;
    self.computerCharacter.health = 100;
    self.computerCharacterWrapper.physicsBody.dynamic = YES;
    self.computerCharacterWrapper.physicsBody.mass = 1;
    self.computerCharacterWrapper.physicsBody.friction = 1;
    self.computerCharacterWrapper.physicsBody.allowsRotation = NO;
    self.computerCharacterWrapper.physicsBody.affectedByGravity = YES;
    self.computerCharacter.named = @"computerCharacter";
    self.computerCharacter.characterName  = @"ken";
    self.computerCharacter.name = @"computerCharacter";
    
    self.computerCharacter.xScale = -1 *self.characterScale;
    self.computerCharacter.yScale = self.characterScale;
    self.computerCharacter.anchorPoint = CGPointMake(0.5, 0);
    self.computerCharacter.currentState = @"idle";
    idleAction = [GameScene SKActionAnimationWithCharacterName:@"ken" move:@"idle"];
    [self.computerCharacter runAction:[SKAction repeatAction:idleAction count:100]];
    self.computerCharacterWrapper.position = CGPointMake(CGRectGetMidX(self.frame)+200, CGRectGetMidY(self.frame)-150);
    [self addChild:self.computerCharacterWrapper];
    [self.computerCharacterWrapper addChild:self.computerCharacter];
    self.computerCharacterWrapper.position = CGPointMake(self.computerCharacterWrapper.position.x, self.computerCharacterWrapper.position.y+90);
    [self setIdle];
    
    
    self.mainHealth =[SKShapeNode shapeNodeWithRect:CGRectMake(0, 0, 400, 100)];
    self.mainHealth.position = CGPointMake(100, self.frame.size.height-200);
    self.mainHealth.fillColor = [UIColor greenColor];
    self.mainHealth.xScale = 1;
    [self addChild:self.mainHealth];
    
    
    self.computerHealth=[SKShapeNode shapeNodeWithRect:CGRectMake(0, 0, 400, 100)];
    self.computerHealth.position = CGPointMake(self.frame.size.width - 100, self.frame.size.height-200);
    self.computerHealth.fillColor = [UIColor greenColor];
    self.computerHealth.xScale = -1;
    [self addChild:self.computerHealth];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    /* Called when a touch begins */
    
    for (UITouch *touch in touches) {
        CGPoint location = [touch locationInNode:self];
        SKNode *touchedNode = [self nodeAtPoint:location];
        if(touchedNode){
            if([touchedNode isEqual:self.punchButton]){
                [self.punchButton setTexture: [SKTexture textureWithImageNamed:@"arcade_red_pushed.png"]];
                if ([self.mainCharacter.currentState isEqualToString:@"idle"]|| [self.mainCharacter.currentState isEqualToString:@"walking"]) {
                    self.mainCharacter.currentState = @"attacking";
                    [self.mainCharacter runAction:[GameScene SKActionAnimationWithCharacterName:@"ken" move:@"punch"] completion:^{
                        [self setIdle];
                    }];
                    
                    NSString *path = [[NSBundle mainBundle] pathForResource:@"characterPList" ofType:@"plist"];
                    NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:path];
                    NSDictionary *charDict = [dict valueForKey:@"ken"];
                    int frameOfAttack = [[charDict valueForKey:   @"punchAttackFrame"] intValue];
                    double timePerFrame = [[charDict valueForKey:@"punchFrameRate"]doubleValue];
                    double waitTime = frameOfAttack*timePerFrame - 0.1;
                    int xBodySize = [[charDict valueForKey:@"punchHitBoxX"]intValue];
                    int yBodySize = [[charDict valueForKey:@"punchHitBoxY"]intValue];
                    
                    
                    dispatch_queue_t queue = dispatch_queue_create("physicsBody", NULL);
                    dispatch_async(queue, ^{
                        double endTime = CACurrentMediaTime() + waitTime;
                        while (CACurrentMediaTime() <endTime) {
                        }
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            self.lastBodySize = CGSizeMake(xBodySize, yBodySize);
                            self.mainCharacterWrapper.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(xBodySize *self.characterScale, yBodySize * self.characterScale)];
                            self.mainCharacter.physicsBody.allowsRotation = NO;
                        });
                    });
                }
                if ([self.mainCharacter.currentState isEqualToString:@"crouching"]) {
                    self.mainCharacter.currentState = @"attackingcrouch";
                    [self.mainCharacter runAction:[GameScene SKActionAnimationWithCharacterName:@"ken" move:@"crouchpunch"] completion:^{
                        self.mainCharacter.currentState = @"crouching";
                        [self.mainCharacter runAction:[GameScene SKActionAnimationWithCharacterName:@"ken" move:@"crouch"]];
                    }];
                    
                    NSString *path = [[NSBundle mainBundle] pathForResource:@"characterPList" ofType:@"plist"];
                    NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:path];
                    NSDictionary *charDict = [dict valueForKey:@"ken"];
                    int frameOfAttack = [[charDict valueForKey:   @"crouchpunchAttackFrame"] intValue];
                    double timePerFrame = [[charDict valueForKey:@"crouchpunchFrameRate"]doubleValue];
                    double waitTime = frameOfAttack*timePerFrame - 0.1;
                    int xBodySize = [[charDict valueForKey:@"crouchpunchHitBoxX"]intValue];
                    int yBodySize = [[charDict valueForKey:@"crouchpunchHitBoxY"]intValue];
                    
                    
                    dispatch_queue_t queue = dispatch_queue_create("physicsBody", NULL);
                    dispatch_async(queue, ^{
                        double endTime = CACurrentMediaTime() + waitTime;
                        while (CACurrentMediaTime() <endTime) {
                        }
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            self.lastBodySize = CGSizeMake(xBodySize, yBodySize);
                            self.mainCharacterWrapper.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(xBodySize *self.characterScale, yBodySize * self.characterScale)];
                            self.mainCharacterWrapper.physicsBody.allowsRotation = NO;
                        });
                    });
                }
                
                
            }
            if ([touchedNode isEqual:self.kickButton]) {
                [self.kickButton setTexture:[SKTexture textureWithImageNamed:@"arcade_blue_pushed.png"]];
            }
        }
        
        
        
        
        /*
         SKSpriteNode *sprite = [SKSpriteNode spriteNodeWithImageNamed:@"RyuJumpPunch1.png"];
         sprite.anchorPoint = CGPointMake(0.5, 0);
         sprite.xScale = 2;
         sprite.yScale = 2;
         sprite.position = location;
         
         NSArray *jumpPunch = [NSArray arrayWithObjects:[SKTexture textureWithImage:[UIImage imageNamed:@"RyuJumpPunch1.png"]],[SKTexture textureWithImage:[UIImage imageNamed:@"RyuJumpPunch2.png"]],[SKTexture textureWithImage:[UIImage imageNamed:@"RyuJumpPunch3.png"]],[SKTexture textureWithImage:[UIImage imageNamed:@"RyuJumpPunch4.png"]],[SKTexture textureWithImage:[UIImage imageNamed:@"RyuJumpPunch5.png"]],[SKTexture textureWithImage:[UIImage imageNamed:@"RyuJumpPunch6.png"]],[SKTexture textureWithImage:[UIImage imageNamed:@"RyuJumpPunch7.png"]], nil];
         
         
         SKAction *jumpPunchAction  = [SKAction animateWithTextures: jumpPunch timePerFrame:0.15 resize:YES restore:YES];
         
         //SKAction *action = [SKAction rotateByAngle:M_PI duration:1];
         
         [sprite runAction:[SKAction repeatAction:jumpPunchAction count:1000]];
         
         //[self addChild:sprite];
         */
    }
}

-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    BOOL punchButton = NO, kickButton = NO;
    for (UITouch *touch in touches) {
        CGPoint location = [touch locationInNode:self];
        SKNode *touchedNode = [self nodeAtPoint:location];
        if(touchedNode){
            if([touchedNode isEqual:self.punchButton]){
                punchButton = YES;
                [self.punchButton setTexture: [SKTexture textureWithImageNamed:@"arcade_red_pushed.png"]];
            }
            if ([touchedNode isEqual:self.kickButton]) {
                [self.kickButton setTexture:[SKTexture textureWithImageNamed:@"arcade_blue_pushed.png"]];
                kickButton = YES;
            }
        }
        
    }
    
    if (!punchButton) {
        [self.punchButton setTexture:[SKTexture textureWithImageNamed:@"arcade_red_unpushed.png"]];
    }
    if (!kickButton) {
        [self.punchButton setTexture:[SKTexture textureWithImageNamed:@"arcade_red_unpushed.png"]];
    }
    
    
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    for (UITouch *touch in touches) {
        CGPoint location = [touch locationInNode:self];
        SKNode *touchedNode = [self nodeAtPoint:location];
        if(touchedNode){
            if([touchedNode isEqual:self.punchButton]){
                [self.punchButton setTexture: [SKTexture textureWithImageNamed:@"arcade_red_unpushed.png"]];
            }
            if ([touchedNode isEqual:self.kickButton]) {
                [self.kickButton setTexture:[SKTexture textureWithImageNamed:@"arcade_blue_unpushed.png"]];
            }
        }
        
    }
}

-(void) setIdle
{
    [self.mainCharacterWrapper removeAllActions];
    [self.mainCharacter removeAllActions];
    self.mainCharacter.currentState = @"idle";
    [self.mainCharacter runAction:[SKAction repeatActionForever:[GameScene SKActionAnimationWithCharacterName:@"ken" move:@"idle"]]];
    
    self.mainCharacterWrapper.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(33*self.characterScale, 82*self.characterScale)];
    self.mainCharacterWrapper.physicsBody = self.mainCharacterWrapper.physicsBody;
    self.mainCharacterWrapper.physicsBody.allowsRotation = NO;
    
}

-(void)update:(CFTimeInterval)currentTime {
    NSLog(@"X - %f Y - %f, X-%f Y%f state - %@",self.computerCharacterWrapper.position.x,self.computerCharacterWrapper.position.y,self.mainCharacterWrapper.position.x,self.mainCharacterWrapper.position.y,self.mainCharacter.currentState);
    if (self.joystick.velocity.x >30  && self.joystick.velocity.y <35) {
        if([self.mainCharacter.currentState isEqualToString:@"idle"]||[self.mainCharacter.currentState isEqualToString:@"crouching"]){
            [self.mainCharacter removeAllActions];
            self.mainCharacter.xScale = self.characterScale;
            self.mainCharacter.yScale = self.characterScale;
            self.mainCharacter.currentState = @"walking";
            SKAction *walkingAction = [GameScene SKActionAnimationWithCharacterName:@"ken" move:@"walking"];
            SKAction *movingWalkingAction = [SKAction moveByX:10000 y:0 duration:50];
            [self.mainCharacter runAction:[SKAction repeatActionForever:walkingAction]];
            [self.mainCharacterWrapper runAction:movingWalkingAction];
        }
    }else if(self.joystick.velocity.x < -30 &&self.joystick.velocity.y <35 ){
        if([self.mainCharacter.currentState isEqualToString:@"idle"]||[self.mainCharacter.currentState isEqualToString:@"crouching"]){
            [self.mainCharacter removeAllActions];
            self.mainCharacter.xScale = -1 * self.characterScale;
            self.mainCharacter.yScale = self.characterScale;
            self.mainCharacter.currentState = @"walking";
            SKAction *walkingAction = [GameScene SKActionAnimationWithCharacterName:@"ken" move:@"walking"];
            SKAction *movingWalkingAction = [SKAction moveByX:-10000 y:0 duration:50];
            [self.mainCharacter runAction:[SKAction repeatActionForever:walkingAction]];
            [self.mainCharacterWrapper runAction:movingWalkingAction];
        }
    }else if(self.joystick.velocity.x >-20 && self.joystick.velocity.x <20 && self.joystick.velocity.y >-55&&self.joystick.velocity.y<35){
        if ([self.mainCharacter.currentState isEqualToString:@"walking"]||[self.mainCharacter.currentState isEqualToString:@"crouching"]) {
            [self setIdle];
        }
    }else if(self.joystick.velocity.y < -57.25){
        if ([self.mainCharacter.currentState isEqualToString:@"idle"]|| [self.mainCharacter.currentState isEqualToString:@"walking"]) {
            [self.mainCharacter removeAllActions];
            [self.mainCharacterWrapper removeAllActions];
            self.mainCharacter.currentState = @"crouching";
            [self.mainCharacter runAction:[GameScene SKActionAnimationWithCharacterName:@"ken" move:@"crouch"]];
        }
    }else if (self.joystick.velocity.y > 35) {
        if ([self.mainCharacter.currentState isEqualToString:@"idle"]||[self.mainCharacter.currentState isEqualToString:@"walking"]) {
            [self.mainCharacter removeAllActions];
            if ((self.joystick.velocity.x > 30 &&self.joystick.velocity.x < 60)|| (self.joystick.velocity.x < -30 &&self.joystick.velocity.x > -60)) {
                [self.mainCharacter removeAllActions];
                int scale;
                if (self.joystick.velocity.x <0) {
                    scale = -1;
                }else{
                    scale = 1;
                }
                SKAction *movingWalkingAction = [SKAction moveByX:scale*10000 y:0 duration:50];
                [self.mainCharacterWrapper removeAllActions];
                [self.mainCharacterWrapper runAction:movingWalkingAction];
                self.mainCharacter.currentState = @"forwardjump";
                self.mainCharacterWrapper.physicsBody.dynamic = YES;
                
                [self.mainCharacterWrapper.physicsBody applyImpulse:CGVectorMake(0, 700)];
                [self.mainCharacter runAction:[GameScene SKActionAnimationWithCharacterName:@"ken" move:@"forwardjump"]completion:^{
                    self.mainCharacterWrapper.physicsBody.dynamic = NO;
                    dispatch_queue_t queue = dispatch_queue_create("waitingQueue", NULL);
                    dispatch_async(queue, ^{
                        [self.mainCharacter setTexture:[SKTexture textureWithImageNamed:@"ken_forwardjump_6.png"]];
                        while (self.mainCharacterWrapper.position.y>430) {
                        }
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self setIdle];
                        });
                        
                        
                    });
                    
                    
                    
                }];
            }else{
                [self.mainCharacterWrapper removeAllActions];
                [self.mainCharacter removeAllActions];
                self.mainCharacter.currentState = @"jumping";
                self.mainCharacterWrapper.physicsBody.dynamic = YES;
                
                [self.mainCharacterWrapper.physicsBody  applyImpulse:CGVectorMake(0, 700)];
                [self.mainCharacter runAction:[GameScene SKActionAnimationWithCharacterName:@"ken" move:@"jump"]completion:^{
                    self.mainCharacterWrapper.physicsBody.dynamic = NO;
                    dispatch_queue_t queue = dispatch_queue_create("waitingQueue", NULL);
                    dispatch_async(queue, ^{
                        [self.mainCharacter setTexture:[SKTexture textureWithImageNamed:@"ken_jump_6.png"]];
                        while (self.mainCharacterWrapper.position.y>430) {
                        }
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self setIdle];
                        });
                        
                        
                    });
                    
                }];
            }
        }
        
    }else if(self.joystick.velocity.y >-57.25 && self.joystick.velocity.y < 35){
        [self setIdle];
    }
    
    
    
    ///COMPUTER CODE
#pragma mark - COMPUTER CONTROL CODE
    
    if (self.mainCharacterWrapper.position.x < self.computerCharacterWrapper.position.x -100) {
        if(arc4random() %150 == 0){
            if ([self.computerCharacter.currentState isEqualToString:@"idle"]) {
                
            
            [self walkCharacter:self.computerCharacter withWrapper:self.computerCharacterWrapper];
            }
        }
    }
    if (ABS(self.mainCharacterWrapper.position.x- self.computerCharacterWrapper.position.x) <100) {
        //[self.computerCharacterWrapper removeAllActions];
        //[self.computerCharacter removeAllActions];
        //[self idleCharacter:self.computerCharacter withWrapper:self.computerCharacterWrapper];
    }
    
    if (ABS(self.mainCharacterWrapper.position.x- self.computerCharacterWrapper.position.x) <100) {
        if (arc4random()%60 ==0) {
            //[self attackWithCharacter:self.computerCharacter withWrapper:self.computerCharacterWrapper withMove:@"punch"];
        }
        
    }
    
    //NSLog(@"Hitbox X - %f  Y- %f",self.lastBodySize.width ,self.lastBodySize.height);
    //NSLog(@"X = %f, Y = %f",self.joystick.velocity.x,self.joystick.velocity.y);
    /* Called before each frame is rendered */
}

+(SKAction *)SKActionAnimationWithCharacterName:(NSString *)characterName move:(NSString *)move
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"characterPList" ofType:@"plist"];
    NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:path];
    NSDictionary *charDict = [dict valueForKey:characterName];
    double frameRate = [[charDict valueForKey:[move stringByAppendingString:@"FrameRate"]] doubleValue];
    return  [SKAction animateWithTextures:[GameScene arrayOfSKTexturesCharacterName:characterName move:move] timePerFrame:frameRate resize:YES restore:NO];
}

+(NSArray *)arrayOfSKTexturesCharacterName:(NSString*)characterName move: (NSString*)move
{
    NSMutableArray *array = [[NSMutableArray alloc]init];
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"characterPList" ofType:@"plist"];
    NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:path];
    NSDictionary *charDict = [dict valueForKey:characterName];
    NSString *frameNumberKey =[move stringByAppendingString:@"Frames"];
    int numberOfFrames =[[charDict objectForKey:frameNumberKey]intValue];
    //NSLog(@"%@ - %d",frameNumberKey,numberOfFrames);
    for (int i=0; i < numberOfFrames ; i++) {
        NSString *imageName =[NSString stringWithFormat:@"%@_%@_%d.png",characterName, move, i];
        //NSLog(@"ImageName - %@",imageName);
        SKTexture *temp = [SKTexture textureWithImageNamed:imageName];
        [array addObject:temp];
    }
    
    return array;
}

-(void) didBeginContact:(SKPhysicsContact *)contact
{
    
    NSLog(@"CONTACT");
    SKNode *tempfirstNode,*tempsecondNode;
    
    
    tempfirstNode = (SKNode *)contact.bodyA.node ;
    tempsecondNode = (SKNode*)contact.bodyB.node;
    if (contact.bodyA.categoryBitMask ==opposingCharacterCategory && contact.bodyB.categoryBitMask ==mainCharacterCategory) {
        SKNode*temp = tempfirstNode;
        tempfirstNode = tempsecondNode;
        tempsecondNode = temp;
    }
    SKCharacter *firstNode = (SKCharacter * )[tempfirstNode.children firstObject],*secondNode = (SKCharacter *)[tempsecondNode.children firstObject];
    
    if ([firstNode isKindOfClass:[SKCharacter class]]&& [secondNode isKindOfClass:[SKCharacter class]]) {
        NSLog(@"state1 - %@ , state2 - %@",firstNode.currentState,secondNode.currentState);
        NSLog(@"Contact is Made");
        
        NSString *state1=firstNode.currentState, *state2 = secondNode.currentState;
        
        if (state1.length>9) {
            state1 = [state1 substringToIndex:9];
        }
        if (state2.length>9) {
            state2 = [state2 substringToIndex:9];
        }
        
        
        if ([state1 isEqualToString:@"attacking"] && ![state2 isEqualToString:@"attacking"]) {
            [self hitCharacter:secondNode withWrapper:tempsecondNode];
        }else if (![state1 isEqualToString:@"attacking"]&& [state2 isEqualToString:@"attacking"])
        {
            [self hitCharacter:firstNode withWrapper:tempfirstNode];
        }else if([state1 isEqualToString:@"attacking"]&& [state2 isEqualToString:@"attacking"]){
            [self hitCharacter:firstNode withWrapper:tempfirstNode];
            [self hitCharacter:secondNode withWrapper:tempsecondNode];
        }
        
    }
    
    
    
}
-(void)idleCharacter: (SKCharacter *)character withWrapper: (SKNode *)wrapper
{
    [character removeAllActions];
    [wrapper removeAllActions];
    
    wrapper.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(33*self.characterScale, 82*self.characterScale)];
    wrapper.physicsBody = wrapper.physicsBody;
    wrapper.physicsBody.allowsRotation = NO;
    wrapper.physicsBody.categoryBitMask = opposingCharacterCategory;
    wrapper.physicsBody.contactTestBitMask = opposingCharacterCategory|mainCharacterCategory;
    wrapper.physicsBody.dynamic = YES;

    
    character.xScale = -self.characterScale;
    character.yScale = self.characterScale;
    character.currentState = @"idle";
    SKAction *idleAction = [GameScene SKActionAnimationWithCharacterName:@"ken" move:@"idle"];
    [character runAction:[SKAction repeatAction:idleAction count:1]completion:^{
        if ([character.currentState isEqualToString:@"idle"]) {
            [self idleCharacter:character withWrapper:wrapper];
        }
    }];
    [wrapper runAction:idleAction];
}
-(void)walkCharacter: (SKCharacter *)character withWrapper: (SKNode *)wrapper
{
    [character removeAllActions];
    [wrapper removeAllActions];
    character.xScale = -self.characterScale;
    character.yScale = self.characterScale;
    character.currentState = @"walking";
    SKAction *walkingAction = [GameScene SKActionAnimationWithCharacterName:@"ken" move:@"walking"];
    SKAction *movingWalkingAction = [SKAction moveByX:-10000 y:0 duration:50];
    [character runAction:[SKAction repeatAction:walkingAction count:1]completion:^{
        [wrapper runAction:[SKAction moveToX:wrapper.position.x+20 duration:0.0001]];
        if(arc4random()%3==0){
        [self walkCharacter:character withWrapper:wrapper];
        }else{
            character.currentState = @"idle";
            [wrapper removeAllActions];
        //[self idleCharacter:character withWrapper:wrapper];
        }
        
    }];
    [wrapper runAction:movingWalkingAction];
    
    
}

-(void)attackWithCharacter: (SKCharacter *)character withWrapper: (SKNode*)wrapper withMove:(NSString*)move
{
    if ([character.currentState isEqualToString:@"idle"] || [character.currentState isEqualToString:@"walking"]) {
        character.currentState = @"attacking";
        [character runAction:[GameScene SKActionAnimationWithCharacterName:@"ken" move:move] completion:^{
            [self idleCharacter:character withWrapper:wrapper];
        }];
        
        NSString *path = [[NSBundle mainBundle] pathForResource:@"characterPList" ofType:@"plist"];
        NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:path];
        NSDictionary *charDict = [dict valueForKey:@"ken"];
        int frameOfAttack = [[charDict valueForKey:  [NSString stringWithFormat: @"%@AttackFrame",move]] intValue];
        double timePerFrame = [[charDict valueForKey:[NSString stringWithFormat: @"%@AttackFrameRate",move]]doubleValue];
        double waitTime = frameOfAttack*timePerFrame - 0.1;
        int xBodySize = [[charDict valueForKey:[NSString stringWithFormat: @"%@HitBoxX",move]]intValue];
        int yBodySize = [[charDict valueForKey:[NSString stringWithFormat: @"%@HitBoxY",move]]intValue];
        
        
        dispatch_queue_t queue2 = dispatch_queue_create("physicsBody2", NULL);
        dispatch_async(queue2, ^{
            double endTime = CACurrentMediaTime() + waitTime;
            while (CACurrentMediaTime() <endTime) {
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                wrapper.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(xBodySize *self.characterScale, yBodySize * self.characterScale)];
                character.physicsBody.allowsRotation = NO;
                wrapper.physicsBody.categoryBitMask = opposingCharacterCategory;
                wrapper.physicsBody.contactTestBitMask = opposingCharacterCategory|mainCharacterCategory;
                wrapper.physicsBody.dynamic = YES;
                
            });
        });

    }
    
}


-(void)hitCharacter:(SKCharacter *)character withWrapper:(SKNode*)wrapper{
    wrapper.physicsBody.dynamic = YES;
    int direction;
    if([character.named isEqualToString:@"mainCharacter"]){
        direction = -1;
    }else{
        direction = 1;
    }
    NSString *move;
    
    character.health -=4;
    
    
    if (character.health <=0) {
        SKLabelNode *youWin = [SKLabelNode labelNodeWithText:@"You Win!"];
        youWin.fontSize = 80.0f;
        youWin.fontColor = [UIColor blueColor];
        youWin.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
        [self addChild:youWin];
        self.paused = YES;
        character.health = 0;
    }
    if ([character.named isEqualToString:@"computerCharacter"]) {
            self.computerHealth.xScale = -(character.health/100.0);
        
    }else{
            self.mainHealth.xScale = (character.health/100.0);
    }

    
    if ([character.currentState isEqualToString:@"attacking"]||[character.currentState isEqualToString:@"idle"]||[character.currentState isEqualToString:@"walking"]||[character.currentState isEqualToString:@"jumping"]||[character.currentState isEqualToString:@"forwardjump"])
    {
        move = @"hit";
    }else if ([character.currentState isEqualToString:@"crouching"]|| [character.currentState isEqualToString:@"attackingcrouch"]){
        move = @"crouchhit";
    }
    [character runAction:[GameScene SKActionAnimationWithCharacterName:character.characterName move:move]completion:^{
        [self setIdle];
    }];
    [wrapper.physicsBody applyImpulse:CGVectorMake(300 *direction, 0)];
    
    
    
}


@end
