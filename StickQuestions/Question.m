//
//  Question.m
//  StickQuestions
//
//  Created by Avery Lamp on 1/31/15.
//  Copyright (c) 2015 Avery Lamp. All rights reserved.
//

#import "Question.h"

@implementation Question

-(instancetype)initWithQuestion:(NSString *)question withAnswers: (NSMutableArray *)answerArr
{
    self =[super init];
    self.question =    question;
    
    self.answers = answerArr;
    
    return self;
}


@end
