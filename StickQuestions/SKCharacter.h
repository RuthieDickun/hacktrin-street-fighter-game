//
//  SKCharacter.h
//  StickQuestions
//
//  Created by Avery Lamp on 1/31/15.
//  Copyright (c) 2015 Avery Lamp. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface SKCharacter : SKSpriteNode
@property int punchDamage;
@property int kickDamage;
@property int health;
@property BOOL punchOn;
@property BOOL kickOn;
@property NSString* currentState;
@property NSString* named;
@property NSString *characterName;
@end
